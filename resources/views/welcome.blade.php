<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  </head>
  <body>
    <form action="{{route("uploads.store")}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Kelurahan</label>
            <select class="form-select" id="inputGroupSelect01" required name="kelurahan">
                <option value="KEDAUNG">KEDAUNG</option>
                <option value="BENDA BARU">BENDA BARU</option>
                <option value="PAMULANG BARAT">PAMULANG BARAT</option>
                <option value="PAMULANG TIMUR">PAMULANG TIMUR</option>
                <option value="PONDOK CABE UDIK">PONDOK CABE UDIK</option>
                <option value="BAMBUAPUS">BAMBUAPUS</option>
            </select>
        </div>
        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">RT</label>
            <input required name="rt" type="number" class="form-control" maxlength="2" value="" placeholder="" aria-describedby="button-addon1">
        </div>
        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">RW</label>
            <input required name="rw" type="number" class="form-control" maxlength="2" value="" placeholder="" aria-describedby="button-addon1">
        </div>
        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">NIK</label>
            <input required name="nik" type="number" class="form-control" maxlength="16" min="1000000000000000" placeholder="" aria-describedby="button-addon1">
        </div>
        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">KTP</label>
            <input name="ktp" type="file" class="form-control" placeholder="" aria-describedby="button-addon1">
        </div>
        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">KK</label>
            <input name="kk" type="file" class="form-control" placeholder="" aria-describedby="button-addon1">
        </div>
        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">BAST</label>
            <input name="bast" type="file" class="form-control" placeholder="" aria-describedby="button-addon1">
        </div>
        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">keterangan</label>
            <input name="keterangan" type="text" class="form-control" placeholder="" aria-describedby="button-addon1">
        </div>
        <button type="submit">submit</button>
    </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
  </body>
</html>
