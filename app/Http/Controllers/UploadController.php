<?php

namespace App\Http\Controllers;

use App\Models\Upload;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!empty($request->ktp)) {
            $file_store_ktp = $request->nik."_ktp".".".$request->ktp->getClientOriginalExtension();
            $store_ktp = $request->ktp->move("storage\\".$request->kelurahan."\\RTRW\\RT.".$request->rt." RW.".$request->rw."\\".$request->nik, $file_store_ktp);
            $path_ktp = "storage\\".$request->kelurahan."\\RTRW\\RT.".$request->rt." RW.".$request->rw."\\".$request->nik."\\".$file_store_ktp;
        }else {
            $path_ktp = null;
        }
        if (!empty($request->kk)) {
            $file_store_kk = $request->nik."_kk".".".$request->kk->getClientOriginalExtension();
            $store_kk = $request->kk->move("storage\\".$request->kelurahan."\\RTRW\\RT.".$request->rt." RW.".$request->rw."\\".$request->nik, $file_store_kk);
            $path_kk = "storage\\".$request->kelurahan."\\RTRW\\RT.".$request->rt." RW.".$request->rw."\\".$request->nik."\\".$file_store_kk;
        }else {
            $path_kk = null;
        }
        if (!empty($request->bast)) {
            $file_store_bast = $request->nik."_bast".".".$request->bast->getClientOriginalExtension();
            $store_bast = $request->bast->move("storage\\".$request->kelurahan."\\RTRW\\RT.".$request->rt." RW.".$request->rw."\\".$request->nik, $file_store_bast);
            $path_bast = "storage\\".$request->kelurahan."\\RTRW\\RT.".$request->rt." RW.".$request->rw."\\".$request->nik."\\".$file_store_bast;
        }else {
            $path_bast = null;
        }
        $save = new Upload();
        $save->kelurahan    = $request->kelurahan;
        $save->rt           = $request->rt;
        $save->rw           = $request->rw;
        $save->nik          = $request->nik;
        $save->keterangan   = $request->keterangan;
        $save->ktp          = $path_ktp;
        $save->kk          = $path_kk;
        $save->bast          = $path_bast;
        $save->save();

        return view('welcome');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Upload  $upload
     * @return \Illuminate\Http\Response
     */
    public function show(Upload $upload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Upload  $upload
     * @return \Illuminate\Http\Response
     */
    public function edit(Upload $upload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Upload  $upload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Upload $upload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Upload  $upload
     * @return \Illuminate\Http\Response
     */
    public function destroy(Upload $upload)
    {
        //
    }
}
